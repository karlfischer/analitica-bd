
/* DDL modelo de datos de KPI Exposición */

CREATE TABLE `disponibilidad_ftth` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `anyomes` int(11) NOT NULL,
  `zona` varchar(100) NOT NULL,
  `total_servicio` int(11) DEFAULT NULL COMMENT 'Total de parque ftth',
  `total_incidentes` int(11) NOT NULL COMMENT 'Total de incidentes cerrados',
  `total_minutos` int(11) NOT NULL COMMENT 'total de minutos indisponibilidad',
  `total_afectados` int(11) NOT NULL COMMENT 'Total de clientes afectados',
  `uptime` float DEFAULT NULL COMMENT 'Uptime servicio',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=25 DEFAULT CHARSET=latin1;

CREATE TABLE `provision_cliente` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `zona` varchar(150) NOT NULL,
  `total_clientes` int(11) NOT NULL,
  `fecha` date NOT NULL,
  PRIMARY KEY (`id`),
  KEY `provision_cliente_zona_IDX` (`zona`,`fecha`) USING BTREE
) ENGINE=MyISAM AUTO_INCREMENT=63 DEFAULT CHARSET=latin1;

CREATE TABLE `detalle_disponibilidad_ftth` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `id_incidente` varchar(15) NOT NULL,
  `fecha_inicio` datetime NOT NULL,
  `fecha_fin` datetime DEFAULT NULL,
  `duracion` int(11) DEFAULT NULL,
  `zona` varchar(150) NOT NULL,
  `cerrado` tinyint(1) NOT NULL,
  `anyomes` int(11) NOT NULL,
  `fecha_carga` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `clientes_afectados` int(11) NOT NULL DEFAULT 0,
  `total_alarmas` int(11) NOT NULL,
  `duracion_estimada` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `disponibilidad_ftth_id_incidente_IDX` (`id_incidente`) USING BTREE,
  KEY `disponibilidad_ftth_zona_IDX` (`zona`) USING BTREE
) ENGINE=MyISAM AUTO_INCREMENT=3201 DEFAULT CHARSET=latin1;

