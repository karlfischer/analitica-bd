SELECT pc.zona, pc.total_clientes, D.Tot_Incidente, D.Tot_Minutos, D.Tot_Cli_af 
FROM provision_cliente pc 
INNER JOIN (
	SELECT
	    df.zona AS Zona,
	    COUNT(df.id_incidente) AS Tot_Incidente,
	    SUM(df.duracion) AS Tot_Minutos,
	    SUM(df.clientes_afectados) as Tot_Cli_af
	FROM
	    kpi.detalle_disponibilidad_ftth df
	WHERE DATE(df.fecha_inicio) = '2021-01-01' 
	GROUP BY
	    df.zona
) AS D ON D.zona = pc.zona and pc.fecha = '2021-01-01'

