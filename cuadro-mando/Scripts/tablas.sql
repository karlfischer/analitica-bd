/* Modelo de datos de ingesta de Cassandra */

create table incident_ftth (
	upload_date timestamp,
	yearmonth int,
	incident_number text,
	description text,
	submit_date timestamp,
	last_modified_date timestamp,
	resolution_date timestamp,
	status int,
	cause text,
	real_date date,
	zona text,
	fatem boolean,
	reported_date timestamp,
	PRIMARY KEY ((status, yearmonth), submit_date)
) WITH CLUSTERING ORDER by (submit_date DESC);

create table alarm_remedy (
	incident_number text,	
	yearmonth int,
	upload_date timestamp,
	alarm_start timestamp,
	alarm_end timestamp,
	duration smallint,
	clients smallint,
	canceled boolean,
	lt text,
	olt text,
	pon text,
	cap text,
	cto text,
	PRIMARY KEY ((incident_number), upload_date)
) WITH CLUSTERING ORDER by (upload_date DESC);

create table alarm_fatem (
	id_alarma text,
	yearmonth int,
	incident_number text,
	upload_date timestamp,
	alarm_start timestamp,
	alarm_end timestamp,
	alarm_est_end timestamp,
	duration smallint,
	clients smallint,
	cleared boolean,
	lt text,
	olt text,
	pon text,
	PRIMARY KEY ((id_alarma), upload_date, incident_number)
) WITH CLUSTERING ORDER by (upload_date DESC);

create table osp (
	lt text,
	olt text,
	pon text,
	cto text,
	linea text,
	upload_date timestamp,
	PRIMARY KEY ((lt, olt, pon), upload_date)
) WITH CLUSTERING ORDER by (upload_date DESC);

CREATE TABLE incident_note (
	upload_date timestamp,
	incident_number text,
	create_date timestamp,
	modified_date timestamp,
	detailed_description text,
	PRIMARY KEY ((incident_number), modified_date)
) WITH CLUSTERING ORDER by (modified_date DESC);