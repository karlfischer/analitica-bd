-- **** Migración motor BD a ARIA ****

-- Show metadata database/tables/columns
SELECT TABLE_SCHEMA,
       TABLE_NAME,
       CCSA.CHARACTER_SET_NAME AS DEFAULT_CHAR_SET,
       COLUMN_NAME,
       COLUMN_TYPE,
       C.CHARACTER_SET_NAME,
       C.COLLATION_NAME,
       ENGINE
  FROM information_schema.TABLES AS T
  JOIN information_schema.COLUMNS AS C USING (TABLE_SCHEMA, TABLE_NAME)
  JOIN information_schema.COLLATION_CHARACTER_SET_APPLICABILITY AS CCSA
       ON (T.TABLE_COLLATION = CCSA.COLLATION_NAME)
 WHERE TABLE_SCHEMA=SCHEMA()
   AND C.DATA_TYPE IN ('enum', 'varchar', 'char', 'text', 'mediumtext', 'longtext')
 ORDER BY TABLE_SCHEMA,
          TABLE_NAME,
          COLUMN_NAME;

-- Show system variables
SHOW VARIABLES WHERE Variable_name LIKE 'character_set_%' OR Variable_name LIKE 'collation%';

-- Migrar DATABASE

SET collation_connection = 'utf8mb4_general_ci';
ALTER DATABASE ingesta CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci;
          

-- 1) Migrar INDICES
-- Tablas que tiene indices muy largos e ineficientes. Se debe cambiar el tip ode dato
alter table poligonos_agencias modify NOMBRE_AGE varchar(30) null;
drop index incident_note_fatem_description_index on i_incident_note_fatem;
drop index i_eqpt_osp_avba_idavba_index on i_eqpt_osp_avba;

-- 2) Migrar CHARSET y COLLATE

SELECT CONCAT('ALTER TABLE ',TABLE_NAME,' CONVERT TO CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci;')
 FROM INFORMATION_SCHEMA.TABLES
WHERE table_schema = 'ingesta';

-- 3) Migrar Engine

SELECT CONCAT('ALTER TABLE ',TABLE_NAME,' ENGINE=Aria;')
 FROM INFORMATION_SCHEMA.TABLES
 WHERE ENGINE='InnoDB'
 AND table_schema = 'ingesta';