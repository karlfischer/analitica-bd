create table alarm_fatem
(
	id_alarm bigint not null
		primary key,
	yearmonth int not null,
	upload_date timestamp default current_timestamp() not null,
	alarm_start timestamp not null,
	alarm_end timestamp null,
	duration int null,
	clients int null,
	cleared tinyint(1) not null,
	lt varchar(10) not null,
	olt varchar(10) null,
	pon varchar(10) not null,
	updated_date timestamp default current_timestamp() not null on update current_timestamp()
);

create table alarm_fatem_pr
(
	incident_number text null,
	id_alarm bigint null,
	alarm_start datetime null,
	alarm_end text null,
	clients bigint null,
	duration double null,
	incident_status bigint null
);

create table alarm_fatem_pr_v2
(
	incident_number varchar(15) null,
	id_alarm bigint null,
	alarm_start datetime null,
	alarm_end timestamp default current_timestamp() null,
	clients int null,
	duration int null,
	incident_status int null
);

create table incident_ftth_baftc
(
	upload_date timestamp default current_timestamp() not null,
	yearmonth int not null,
	incident_number varchar(15) not null
		primary key,
	description varchar(1000) null,
	submit_date datetime null,
	last_modified_date datetime null,
	status smallint null,
	cause text null,
	real_start_date datetime null,
	zone varchar(200) null,
	reported_date datetime null,
	resolution_date datetime null,
	close_date datetime null
);

create table incident_ftth_fatem
(
	upload_date timestamp default current_timestamp() not null,
	yearmonth int not null,
	incident_number varchar(15) not null
		primary key,
	description varchar(1000) null,
	submit_date datetime null,
	last_modified_date datetime null,
	status smallint null,
	cause text null,
	real_start_date datetime null,
	zone varchar(200) null,
	reported_date datetime null,
	resolution_date datetime null,
	close_date datetime null
);

create index incident_ftth_fatem_status_index
	on incident_ftth_fatem (status);

create index incident_ftth_fatem_yearmonth_index
	on incident_ftth_fatem (yearmonth);

create table incident_ftth_other
(
	upload_date timestamp default current_timestamp() not null,
	yearmonth int not null,
	incident_number varchar(15) not null
		primary key,
	description varchar(1000) null,
	submit_date datetime null,
	last_modified_date datetime null,
	status smallint null,
	cause text null,
	real_start_date datetime null,
	zone varchar(200) null,
	reported_date datetime null,
	resolution_date datetime null,
	close_date datetime null
);

create index incident_ftth_other_status_index
	on incident_ftth_other (status);

create index incident_ftth_other_yearmonth_index
	on incident_ftth_other (yearmonth);

create table incident_note
(
	incident_number varchar(15) not null,
	created_date datetime not null,
	modified_date datetime not null,
	detailed_description longtext not null,
	upload_date timestamp default current_timestamp() not null,
	description varchar(255) not null,
	primary key (incident_number, created_date)
);

create index incident_note_description_index
	on incident_note (description);

create index incident_note_incident_number_index
	on incident_note (incident_number);

create index incident_note_modified_date_index
	on incident_note (modified_date);

create table incident_note_fatem
(
	incident_number varchar(15) not null,
	created_date datetime not null,
	modified_date datetime not null,
	detailed_description longtext not null,
	upload_date timestamp default current_timestamp() not null,
	description varchar(255) not null,
	primary key (incident_number, created_date)
);

create index incident_note_fatem_description_index
	on incident_note_fatem (description);

create index incident_note_fatem_incident_number_index
	on incident_note_fatem (incident_number);

create index incident_note_fatem_modified_date_index
	on incident_note_fatem (modified_date);

create table osp
(
	lt varchar(10) not null,
	olt varchar(10) not null,
	pon varchar(10) not null,
	cto varchar(50) null,
	linea varchar(100) not null,
	upload_date timestamp default current_timestamp() not null
)
comment 'inventario osp';

create index osp_lt_olt_pon_cto_index
	on osp (lt, olt, pon, cto);

create index osp_lt_olt_pon_index
	on osp (lt, olt, pon);

